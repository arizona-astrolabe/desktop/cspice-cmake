/* 
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona.
 * 
 * This file is part of Astrolabe.
 * 
 * Astrolabe is free software: you can redistribute it and/or modify it under the 
 * terms of the GNU General Public License as published by the Free Software 
 * Foundation, either version 3 of the License, or (at your option) any later 
 * version.
 * 
 * Astrolabe is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with 
 * Astrolabe. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "SpiceUsr.h"
#include "greatest.h"

TEST version_string() {
  const char *vrsn = tkvrsn_c("TOOLKIT");
  ASSERT_STR_EQ("CSPICE_N0067", vrsn);
  PASS();
}

// Convert from UTC time to TDB (Barycentric Dynamical Time, aka Ephemeris Time).
// Expected value from https://wgc.jpl.nasa.gov:8443/webgeocalc/ *TimeConversion
TEST utc2et() {
  const char *utc = "2024-04-16-11:59:59";
  double et;
  kclear_c();
  furnsh_c("../test/naif0012.tls");
  utc2et_c(utc, &et);
  kclear_c();
  ASSERT_IN_RANGE(766540868.185616, et, 0.000001);
  PASS();
}

GREATEST_MAIN_DEFS();

int main(int argc, char *argv[]) {
  GREATEST_MAIN_BEGIN();
  RUN_TEST(version_string);
  RUN_TEST(utc2et);
  GREATEST_MAIN_END();
  return 0;
}