# cspice+cmake

This project contains code to build the C language version of NAIF SPICE with
CMake, outputing shared libraries for multiple platforms. Each output platform
must be built on its own platform, as cross-compilation isn't yet supported.

NAIF SPICE code is downloaded before building and isn't distributed here.

[vcpkg](https://vcpkg.io) is used to get a unit test package. It's linked here
as a Git submodule, so you'll want to clone accordingly:

```
git clone https://gitlab.com/arizona-astrolabe/desktop/cspice-cmake.git --recurse-submodules
```

To build for Windows and Linux on x86_64, do:
```
cd cspice-cmake/build
cmake ..
make
```

To build a universal binary for macOS, do:
```
cd cspice-cmake/build
cmake -DCMAKE_OSX_ARCHITECTURES="arm64;x86_64" ..
make
```

After your build is complete, you will have files in your build directory
as well as in `cspice-cmake/lib`. 

---

Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
of Arizona.

This file is part of Astrolabe.

Astrolabe is free software: you can redistribute it and/or modify it under the 
terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version.

Astrolabe is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
Astrolabe. If not, see <https://www.gnu.org/licenses/>.

![GNU General Public License v3 logo](gplv3-with-text-136x68.png)
